# Jade version UPHF

Ce projet présente la mise à niveau de Jade par Emmnanuel ADAM (LAMIH/INSA HdF/UPHF), en bénéficiant d'aides d'étudiants de l'UPHF (Constant ABRAHAM & Théo BONHOMME).

Ce projet reprend donc la célèbre plateforme [Jade v4.5](https://jade.tilab.com/), stoppée en 2017, en la rendant compatible avec les versions récentes de Java (17).
Un très grand merci à ces développeurs pour leur important travail de qualité qui a permis à de très nombreuses personnes de développer des projets basés sur des SMA.

En plus de bénéficier d'un traitement plus sain et plus rapide grâce à l'utilisation des dernières classes de Java pour la gestion de listes, et., des classes et fonctionnalités ont été ajoutées afin de simplifier la création d'agents et leurs communication.


---
# Java Agent Development Framework, UPHF Version.

This project is an update of Jade platform by Emmnanuel ADAM (LAMIH/INSA HdF/UPHF), with the help of two students from UPHF (Constant ABRAHAM & Théo BONHOMME).


This project is a fork of the [Jade framework v4.5](https://jade.tilab.com/), stopped in 2017, motivated by the fact that the team that successfully developed it cannot continue to support the project.

A great thanks to the team that built Jade, and that have made a very nice and helpfull work that allow a lot of people to develop multiagent systems.

